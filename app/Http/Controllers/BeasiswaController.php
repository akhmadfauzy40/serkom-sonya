<?php

namespace App\Http\Controllers;

use App\Models\Beasiswa;
use Illuminate\Http\Request;
use stdClass;

class BeasiswaController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'email' => 'required|email|unique:data_registrasi,email',
            'no_hp' => 'required',
            'semester' => 'required',
            'ipk' => 'required',
            'beasiswa' => 'required',
            'berkas' => 'required|max:5000',
        ]);

        // menyimpan file ke dalam direktori server
        $file = $request->file('berkas');
        $nama_file = time() . "_" . $file->getClientOriginalName();
        $tujuan_upload = $file->store('public');
        $file->move($tujuan_upload, $nama_file);

        $data = new Beasiswa();
        $data->nama = $request->nama;
        $data->email = $request->email;
        $data->no_hp = $request->no_hp;
        $data->semester = $request->semester;
        $data->ipk = $request->ipk;
        $data->beasiswa = $request->beasiswa;
        $data->berkas = $tujuan_upload . '/' . $nama_file;
        $data->save();

        return redirect('form-registrasi')->with('success', 'Data telah berhasil ditambahkan!');
    }

    public function cek_status(Request $request)
    {
        $request->validate([
            'email' => 'required'
        ]);

        $data = Beasiswa::where('email', $request->email)->first();

        if ($data) {
            return view('cek-status', compact('data'));
        } else {
            return back()->withErrors(['Email tidak ditemukan']);
        }
    }

    public function grafik()
    {
        $data = new stdClass();
        $data->rejected = Beasiswa::where('statusAjuan', 'Rejected')->count();
        $data->pending = Beasiswa::where('statusAjuan', 'Pending')->count();
        $data->approved = Beasiswa::where('statusAjuan', 'Approved')->count();

        return view('grafik', compact('data'));
    }
}
