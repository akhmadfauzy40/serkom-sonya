@extends('layout.master')

@push('styles')
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('js/select.dataTables.min.css') }}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('css/vertical-layout-light/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ asset('images/Logo2.png') }}" />
@endpush

@push('scripts')
    <!-- Plugin js for this page -->
    <script src="{{ asset('vendors/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('js/dataTables.select.min.js') }}"></script>

    <!-- End plugin js for this page -->
    <!-- Custom js for this page-->
    <script src="{{ asset('js/dashboard.js') }}"></script>
    <script src="{{ asset('js/Chart.roundedBarCharts.js') }}"></script>
    <!-- End custom js for this page-->

    <!-- Plugin js for this page -->
    <script src="{{ asset('vendors/chart.js/Chart.min.js') }}"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ asset('js/off-canvas.js') }}"></script>
    <script src="{{ asset('js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('js/settings.js') }}"></script>
    <script src="{{ asset('js/todolist.js') }}"></script>
    <!-- endinject -->
@endpush

@section('title')
    Form Pendaftaran
@endsection

@section('content')
    <div class="container-scroller">
        <!-- partial:partials/_navbar.html -->
        <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
            <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
                <a class="navbar-brand brand-logo mr-5" href="/"><img src="{{ asset('images/Logo1.png') }}"
                        class="mr-2" alt="logo" /></a>
                <a class="navbar-brand brand-logo-mini" href="/"><img src="{{ asset('images/Logo2.png') }}"
                        alt="logo" /></a>
            </div>
            <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
                <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                    <span class="icon-menu"></span>
                </button>
                <ul class="navbar-nav navbar-nav-right">
                    <li class="nav-item nav-profile dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                            <img src="{{ asset('images/icon-user.png') }}" alt="profile" />
                        </a>
                    </li>
                </ul>
                <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
                    data-toggle="offcanvas">
                    <span class="icon-menu"></span>
                </button>
            </div>
        </nav>
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <!-- partial -->
            <!-- partial:partials/_sidebar.html -->
            <nav class="sidebar sidebar-offcanvas" id="sidebar">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="/">
                            <i class="icon-grid menu-icon"></i>
                            <span class="menu-title">Home</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/form-registrasi">
                            <i class="icon-layout menu-icon"></i>
                            <span class="menu-title">Form Pendaftaran</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/cek-status">
                            <i class="ti-time menu-icon"></i>
                            <span class="menu-title">Cek Status Pendaftaran</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/grafik-status">
                            <i class="icon-bar-graph menu-icon"></i>
                            <span class="menu-title">Grafik Penerimaan</span>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- partial -->
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-md-12 grid-margin">
                            <div class="row">
                                <div class="col-12 col-xl-8 mb-4 mb-xl-0">
                                    <h3 class="font-weight-bold">Silahkan daftrkan diri anda</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Langkah Pendaftaran</h4>
                                    <!-- Penjelasan Beasiswa Akademik -->
                                    <div class="mb-3">
                                        <p>
                                            Daftarkan diri anda pada form di samping, lengkapi data diri anda setelah itu
                                            klik submit untuk mendaftar. Semoga kalian beruntung untuk mendapatkan beasiswa
                                            yang kalian pilih
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6 grid-margin transparent">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Form Pendaftaran</h4>
                                    <form class="forms-sample" action="/registrasi-beasiswa" method="post"
                                        enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label for="nama">Nama</label>
                                            <input type="text" class="form-control @error('nama') is-invalid @enderror"
                                                id="nama" placeholder="Masukkan Nama Anda" name="nama"
                                                value="{{ old('nama') }}" required>
                                            @error('nama')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="email">E-Mail</label>
                                            <input type="email" class="form-control @error('email') is-invalid @enderror"
                                                id="email" placeholder="Masukkan E-Mail Anda" name="email"
                                                value="{{ old('email') }}" required>
                                            @error('email')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="no_hp">Nomor HP</label>
                                            <input type="number"
                                                class="form-control @error('no_hp') is-invalid @enderror" id="no_hp"
                                                placeholder="Masukkan Nomor Handphone Anda" name="no_hp"
                                                value="{{ old('no_hp') }}" required>
                                            @error('no_hp')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="semester">Semester Saat Ini</label>
                                            <select class="form-control @error('semester') is-invalid @enderror"
                                                id="semester" name="semester" required onchange="updateIPK(this)">
                                                <option selected disabled>Silahkan Pilih Semester Saat Ini</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                            </select>
                                            @error('semester')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="ipk">IPK</label>
                                            <input type="text" class="form-control @error('ipk') is-invalid @enderror"
                                                id="ipk" placeholder="IPK Anda Akan Muncul Otomatis" name="ipk"
                                                value="{{ old('ipk') }}" readonly required>
                                            @error('ipk')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="beasiswa">Pilihan Beasiswa</label>
                                            <select class="form-control @error('beasiswa') is-invalid @enderror"
                                                id="beasiswa" name="beasiswa" required disabled>
                                                <option selected disabled>Silahkan Pilih Beasiswa</option>
                                                <option>Beasiswa Prestasi Akademik</option>
                                                <option>Beasiswa Peningkatan Prestasi Akademik</option>
                                                <option>Beasiswa Bina Desa</option>
                                                <option>Beasiswa Penelitian</option>
                                            </select>
                                            @error('beasiswa')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="berkas">Upload Berkas Syarat</label>
                                            <input class="form-control @error('berkas') is-invalid @enderror"
                                                type="file" id="berkas" name="berkas">
                                            @error('berkas')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                        <button class="btn btn-dark">Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content-wrapper ends -->
                <!-- partial:partials/_footer.html -->
                <footer class="footer">
                    <div class="d-sm-flex justify-content-center justify-content-sm-between">
                        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2021.
                            Carpedia</span>
                        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
                            <i class="ti-heart text-danger ml-1"></i></span>
                    </div>
                </footer>
                <!-- partial -->
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
@endsection

@push('scripts')
    <script>
        function updateIPK(semesterField) {
            var ipkField = document.getElementById('ipk');
            // Menghasilkan nilai random dalam kisaran 2.9 sampai 3.4
            var ipkValue = Math.random() * (3.4 - 2.9) + 2.9;
            ipkField.value = ipkValue.toFixed(2);

            var beasiswaField = document.getElementById('beasiswa');
            if (ipkValue < 3.0) {
                beasiswaField.disabled = true;
                var uploadField = document.getElementById('berkas');
                uploadField.disabled = true;
            } else {
                beasiswaField.disabled = false;
                var uploadField = document.getElementById('berkas');
                uploadField.disabled = false;
            }
        }
    </script>
@endpush
