@extends('layout.master')

@push('styles')
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('js/select.dataTables.min.css') }}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('css/vertical-layout-light/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ asset('images/Logo2.png') }}" />
@endpush

@push('scripts')
    <!-- Plugin js for this page -->
    <script src="{{ asset('vendors/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('js/dataTables.select.min.js') }}"></script>

    <!-- End plugin js for this page -->
    <!-- Custom js for this page-->
    <script src="{{ asset('js/dashboard.js') }}"></script>
    <script src="{{ asset('js/Chart.roundedBarCharts.js') }}"></script>
    <!-- End custom js for this page-->

    <!-- Plugin js for this page -->
    <script src="{{ asset('vendors/chart.js/Chart.min.js') }}"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ asset('js/off-canvas.js') }}"></script>
    <script src="{{ asset('js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('js/settings.js') }}"></script>
    <script src="{{ asset('js/todolist.js') }}"></script>
    <!-- endinject -->
@endpush

@section('title')
    Cek Status Pendaftaran
@endsection

@section('content')
    <div class="container-scroller">
        <!-- partial:partials/_navbar.html -->
        <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
            <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
                <a class="navbar-brand brand-logo mr-5" href="/"><img src="{{ asset('images/Logo1.png') }}"
                        class="mr-2" alt="logo" /></a>
                <a class="navbar-brand brand-logo-mini" href="/"><img src="{{ asset('images/Logo2.png') }}"
                        alt="logo" /></a>
            </div>
            <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
                <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                    <span class="icon-menu"></span>
                </button>
                <ul class="navbar-nav navbar-nav-right">
                    <li class="nav-item nav-profile dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                            <img src="{{ asset('images/icon-user.png') }}" alt="profile" />
                        </a>
                    </li>
                </ul>
                <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
                    data-toggle="offcanvas">
                    <span class="icon-menu"></span>
                </button>
            </div>
        </nav>
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <!-- partial -->
            <!-- partial:partials/_sidebar.html -->
            <nav class="sidebar sidebar-offcanvas" id="sidebar">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="/">
                            <i class="icon-grid menu-icon"></i>
                            <span class="menu-title">Home</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/form-registrasi">
                            <i class="icon-layout menu-icon"></i>
                            <span class="menu-title">Form Pendaftaran</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/cek-status">
                            <i class="ti-time menu-icon"></i>
                            <span class="menu-title">Cek Status Pendaftaran</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/grafik-status">
                            <i class="icon-bar-graph menu-icon"></i>
                            <span class="menu-title">Grafik Penerimaan</span>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- partial -->
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-md-12 grid-margin">
                            <div class="row">
                                <div class="col-12 col-xl-8 mb-4 mb-xl-0">
                                    <h3 class="font-weight-bold">Silahkan Cek Status Pendaftaran Anda</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Langkah Melakukan Pengecekan</h4>
                                    <!-- Penjelasan Beasiswa Akademik -->
                                    <div class="mb-3">
                                        <p>
                                            Silahkan Masukkan Email yang Anda Daftarkan Pada Website ini Lalu klik submit
                                        </p>
                                        <form action="/cek-status" method="post">
                                            @csrf
                                            <div class="form-group">
                                                <label for="email">Email address</label>
                                                <input type="email" class="form-control" id="email"
                                                    placeholder="Masukkan Email Anda" name="email" required>
                                            </div>
                                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6 grid-margin transparent">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Data Registrasi</h4>
                                    <div class="form-group">
                                        <label for="nama">Nama</label>
                                        <input type="text" class="form-control" id="nama"
                                            placeholder="{{ isset($data) ? $data->nama : '' }}" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">E-Mail</label>

                                        <input type="email" class="form-control" id="email"
                                            placeholder="{{ isset($data) ? $data->email : '' }}" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="no_hp">Nomor HP</label>
                                        <input type="number" class="form-control" id="no_hp"
                                            placeholder="{{ isset($data) ? $data->no_hp : '' }}" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="semester">Semester Saat Ini</label>
                                        <select class="form-control" id="semester" disabled>
                                            <option selected disabled></option>
                                            <option {{ isset($data) && $data->semester == '1' ? 'selected' : '' }}>1
                                            </option>
                                            <option {{ isset($data) && $data->semester == '2' ? 'selected' : '' }}>2
                                            </option>
                                            <option {{ isset($data) && $data->semester == '3' ? 'selected' : '' }}>3
                                            </option>
                                            <option {{ isset($data) && $data->semester == '4' ? 'selected' : '' }}>4
                                            </option>
                                            <option {{ isset($data) && $data->semester == '5' ? 'selected' : '' }}>5
                                            </option>
                                            <option {{ isset($data) && $data->semester == '6' ? 'selected' : '' }}>6
                                            </option>
                                            <option {{ isset($data) && $data->semester == '7' ? 'selected' : '' }}>7
                                            </option>
                                            <option {{ isset($data) && $data->semester == '8' ? 'selected' : '' }}>8
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="ipk">IPK</label>
                                        <input type="text" class="form-control" id="ipk"
                                            placeholder="{{ isset($data) ? $data->ipk : '' }}" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="beasiswa">Pilihan Beasiswa</label>
                                        <select class="form-control" id="beasiswa" disabled>
                                            <option selected disabled></option>
                                            <option
                                                {{ isset($data) && $data->beasiswa == 'Beasiswa Prestasi Akademik' ? 'selected' : '' }}>
                                                Beasiswa Prestasi Akademik</option>
                                            <option
                                                {{ isset($data) && $data->beasiswa == 'Beasiswa Peningkatan Prestasi Akademik' ? 'selected' : '' }}>
                                                Beasiswa Peningkatan Prestasi Akademik</option>
                                            <option
                                                {{ isset($data) && $data->beasiswa == 'Beasiswa Bina Desa' ? 'selected' : '' }}>
                                                Beasiswa Bina Desa</option>
                                            <option
                                                {{ isset($data) && $data->beasiswa == 'Beasiswa Peningkatan Kualitas Dosen' ? 'selected' : '' }}>
                                                Beasiswa Peningkatan Kualitas Dosen</option>
                                            <option
                                                {{ isset($data) && $data->beasiswa == 'Beasiswa Penelitian' ? 'selected' : '' }}>
                                                Beasiswa Penelitian</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Berkas Syarat</label><br>
                                        <a target="blank" href="{{ isset($data) ? url($data->berkas) : '#' }}"
                                            class="btn btn-outline-primary btn-icon-text {{ isset($data) ? '' : 'disabled' }}">
                                            <i class="mdi mdi-file-check btn-icon-prepend"></i> KLIK UNTUK LIHAT
                                        </a>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Status Ajuan</label><br>
                                        @if (!isset($data->statusAjuan))
                                            <div class="badge badge-outline-secondary">Silahkan Cari Data Terlebih Dahulu
                                            </div>
                                        @elseif ($data->statusAjuan == 'Approved')
                                            <div class="badge badge-outline-success">Telah di Verifikasi</div>
                                        @elseif ($data->statusAjuan == 'Pending')
                                            <div class="badge badge-outline-warning">Menunggu Verifikasi</div>
                                        @elseif ($data->statusAjuan == 'Rejected')
                                            <div class="badge badge-outline-danger">Ditolak</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content-wrapper ends -->
                <!-- partial:partials/_footer.html -->
                <footer class="footer">
                    <div class="d-sm-flex justify-content-center justify-content-sm-between">
                        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2021.
                            Carpedia</span>
                        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
                            <i class="ti-heart text-danger ml-1"></i></span>
                    </div>
                </footer>
                <!-- partial -->
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
@endsection

@push('scripts')
    <script>
        function updateIPK(semesterField) {
            var ipkField = document.getElementById('ipk');
            // Menghasilkan nilai random dalam kisaran 2.9 sampai 3.4
            var ipkValue = Math.random() * (3.4 - 2.9) + 2.9;
            ipkField.value = ipkValue.toFixed(2);

            var beasiswaField = document.getElementById('beasiswa');
            if (ipkValue < 3.0) {
                beasiswaField.disabled = true;
                var uploadField = document.getElementById('berkas');
                uploadField.disabled = true;
            } else {
                beasiswaField.disabled = false;
                var uploadField = document.getElementById('berkas');
                uploadField.disabled = false;
            }
        }
    </script>
@endpush
