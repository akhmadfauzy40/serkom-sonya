@extends('layout.master')

@push('styles')
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('js/select.dataTables.min.css') }}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('css/vertical-layout-light/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ asset('images/Logo2.png') }}" />
@endpush

@push('scripts')
    <!-- Plugin js for this page -->
    <script src="{{ asset('vendors/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('js/dataTables.select.min.js') }}"></script>

    <!-- End plugin js for this page -->
    <!-- Custom js for this page-->
    <script src="{{ asset('js/dashboard.js') }}"></script>
    <script src="{{ asset('js/Chart.roundedBarCharts.js') }}"></script>
    <!-- End custom js for this page-->

    <!-- Plugin js for this page -->
    <script src="{{ asset('vendors/chart.js/Chart.min.js') }}"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ asset('js/off-canvas.js') }}"></script>
    <script src="{{ asset('js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('js/settings.js') }}"></script>
    <script src="{{ asset('js/todolist.js') }}"></script>
    <!-- endinject -->
@endpush

@section('title')
    Home
@endsection

@section('content')
    <div class="container-scroller">
        <!-- partial:partials/_navbar.html -->
        <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
            <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
                <a class="navbar-brand brand-logo mr-5" href="/"><img
                        src="{{ asset('images/Logo1.png') }}" class="mr-2" alt="logo" /></a>
                <a class="navbar-brand brand-logo-mini" href="/"><img
                        src="{{ asset('images/Logo2.png') }}" alt="logo" /></a>
            </div>
            <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
                <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                    <span class="icon-menu"></span>
                </button>
                <ul class="navbar-nav navbar-nav-right">
                    <li class="nav-item nav-profile dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                            <img src="{{ asset('images/icon-user.png') }}" alt="profile" />
                        </a>
                    </li>
                </ul>
                <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
                    data-toggle="offcanvas">
                    <span class="icon-menu"></span>
                </button>
            </div>
        </nav>
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <!-- partial -->
            <!-- partial:partials/_sidebar.html -->
            <nav class="sidebar sidebar-offcanvas" id="sidebar">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="/">
                            <i class="icon-grid menu-icon"></i>
                            <span class="menu-title">Home</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/form-registrasi">
                            <i class="icon-layout menu-icon"></i>
                            <span class="menu-title">Form Pendaftaran</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/cek-status">
                            <i class="ti-time menu-icon"></i>
                            <span class="menu-title">Cek Status Pendaftaran</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/grafik-status">
                            <i class="icon-bar-graph menu-icon"></i>
                            <span class="menu-title">Grafik Penerimaan</span>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- partial -->
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-md-12 grid-margin">
                            <div class="row">
                                <div class="col-12 col-xl-8 mb-4 mb-xl-0">
                                    <h3 class="font-weight-bold">Selamat Datang di Website Pendaftaran Beasiswa</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Tentang Beasiswa Institut Teknologi Telkom Purwokerto</h4>
                                    <div class="mb-3">
                                        <img src="{{ asset('images/kampus.jpg') }}" alt="" class="img-fluid">
                                    </div>

                                    <!-- Penjelasan Beasiswa Akademik -->
                                    <div class="mb-3">
                                        <h5>Beasiswa Akademik</h5>
                                        <p>
                                            Beasiswa akademik diberikan kepada siswa atau mahasiswa yang memiliki prestasi
                                            akademik tinggi. Jenis beasiswa ini umumnya diberikan berdasarkan pencapaian
                                            akademis seperti nilai rapor, hasil ujian, atau prestasi dalam kegiatan akademis
                                            lainnya.
                                        </p>
                                    </div>

                                    <!-- Penjelasan Beasiswa Non-Akademik -->
                                    <div>
                                        <h5>Beasiswa Non-Akademik</h5>
                                        <p>
                                            Beasiswa non-akademik diberikan kepada individu berdasarkan prestasi atau
                                            keberhasilan di luar lingkup akademis. Jenis beasiswa ini dapat diberikan
                                            berdasarkan prestasi dalam bidang seni, olahraga, keterampilan, atau kontribusi
                                            positif terhadap masyarakat. Beasiswa ini memberikan pengakuan terhadap bakat
                                            dan pencapaian di luar kegiatan akademis.
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6 grid-margin transparent">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Beasiswa yang tersedia</h4>

                                    <!-- Penjelasan Beasiswa Akademik -->
                                    <div class="mb-3">
                                        <h5>Beasiswa Prestasi Akademik : </h5>
                                        <p>
                                            Beasiswa ini diberikan kepada mahasiswa yang memiliki prestasi akademik yang
                                            sangat baik, seperti
                                            memiliki indeks prestasi (IP) di atas rata-rata atau mendapat ranking pada
                                            setiap semester. Besaran
                                            beasiswa ini bervariasi, tergantung pada prestasi akademik yang dicapai.
                                        </p>
                                    </div>

                                    <!-- Penjelasan Beasiswa Non-Akademik -->
                                    <div>
                                        <h5>Beasiswa Peningkatan Prestasi Akademik :</h5>
                                        <p>
                                            Beasiswa ini diberikan kepada mahasiswa yang prestasi akademiknya belum mencapai
                                            standar yang
                                            diharapkan, namun menunjukkan kemajuan yang signifikan dalam meningkatkan
                                            prestasi akademik. Besaran
                                            beasiswa ini juga bervariasi, tergantung pada peningkatan prestasi akademik yang
                                            dicapai.
                                        </p>
                                    </div>

                                    <div>
                                        <h5>Beasiswa Bina Desa :</h5>
                                        <p>
                                            Beasiswa ini diberikan kepada mahasiswa yang berasal dari daerah pedesaan dan
                                            memiliki potensi
                                            akademik yang baik. Tujuannya adalah untuk membantu mahasiswa dari daerah
                                            pedesaan dalam
                                            menyelesaikan studinya di ITTP.
                                        </p>
                                    </div>

                                    <div>
                                        <h5>Beasiswa Penelitian :</h5>
                                        <p>
                                            Beasiswa ini diberikan kepada mahasiswa yang melakukan penelitian yang diakui
                                            secara internasional
                                            dan memiliki potensi untuk memberikan kontribusi dalam bidang ilmu pengetahuan
                                            dan teknologi.
                                            Besaran beasiswa ini bervariasi tergantung pada kualitas penelitian yang
                                            dilakukan.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content-wrapper ends -->
                <!-- partial:partials/_footer.html -->
                <footer class="footer">
                    <div class="d-sm-flex justify-content-center justify-content-sm-between">
                        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2021.
                            Carpedia</span>
                        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
                            <i class="ti-heart text-danger ml-1"></i></span>
                    </div>
                </footer>
                <!-- partial -->
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
@endsection
