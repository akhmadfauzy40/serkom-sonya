<?php

use App\Http\Controllers\BeasiswaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/form-registrasi', function () {
    return view('form');
})->name('form-registrasi');

Route::post('/registrasi-beasiswa', [BeasiswaController::class, 'store']);

Route::get('/cek-status', function(){
    return view('cek-status');
});

Route::post('/cek-status', [BeasiswaController::class, 'cek_status']);
Route::get('/grafik-status', [BeasiswaController::class, 'grafik']);
