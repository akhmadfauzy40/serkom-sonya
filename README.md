## Tutor Instalasi Web

- Buka direktori tempat anda ingin menyimpan project, lalu buka git bash
- ketik perintah di bawah

```bash
git clone https://gitlab.com/akhmadfauzy40/serkom-sonya.git
```

- masuk ke folder project dengan mengetik perintah di bawah ini pada terminal

```bash
cd carpedia
```

- lalu ketik perintah di bawah ini

```bash
composer install
```

- buka vscode dengan mengetik perintah dibawah ini

```bash
code .
```

- pada vscode, dalam folder project buat file baru dengan nama ".env" lalu copy isi dari file ".env.example" dan paste ke dalam ".env"
- ubah keterangan database sesuai dengan yang di inginkan dan jangan lupa di save
- lalu jalankan perintah di bawah ini

```bash
php artisan key:generate
```

- lalu ketik perintah

```bash
php artisan migrate
```

- lalu ketik perintah

```bash
php artisan storage:link
```

- setelah itu jalankan serve dengan mengetik perintah di bawah ini

```bash
php artisan serve
```
